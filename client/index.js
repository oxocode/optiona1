/**
 * This file is used during `npm run dev|start` to load our localhost dev env.
 */
const project = require( '../config/dev.config' );
const server = require( '../config/browser.config' );
const debug = require( 'debug' )( 'app:bin:dev-server' );

debug( `Server is now running at http://localhost:${project.server_port}.` );
debug( 'App ready for development.' );
