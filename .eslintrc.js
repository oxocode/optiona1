module.exports = {
	"env": {
		"browser": true,
		"node": true,
		"es6": true,
		"jest": true,
	},
	"parser": "babel-eslint",
	"plugins": [
		"flowtype",
		"react",
		"jsx-a11y",
		"import"
	],
	"extends": "airbnb",
	"rules": {
		"brace-style": [1, "1tbs"],
		"camelcase": 0,
		"comma-dangle": 0,
		"comma-spacing": 1,
		"import/extensions": ["off"],
		"import/no-unresolved": ["off"],
		"import/no-extraneous-dependencies": ["off"],
		"react/forbid-prop-types": 0,
		"indent": [1, "tab", { "SwitchCase": 1 }],
		"no-tabs": 0,
		"no-mixed-spaces-and-tabs": "error",
		"jsx-a11y/no-static-element-interactions": ["off"],
		"linebreak-style": ["off"],
		"no-multi-spaces": 1,
		"no-plusplus": ["off"],
		"no-spaced-func": 1,
		"no-trailing-spaces": 1,
		"no-undef": 1,
		"no-underscore-dangle": 0,
		"quote-props": [1, "consistent-as-needed"],
		"quotes": [1, "single", "avoid-escape"],
		"react/jsx-filename-extension": ["off"],
		"react/prefer-es6-class": ["off"],
		"react/jsx-indent": [2, "tab"],
		"jsx-a11y/no-marquee": 0,
		"react/prefer-stateless-function": 0,
		"semi-spacing": 1,
		"keyword-spacing": [1],
		"space-before-blocks": [1, "always"],
		"space-before-function-paren": [1, "never"],
		"object-curly-spacing": 1,
		"space-in-parens": [1, "always"],
		"valid-jsdoc": [1, { "requireReturn": false }],
		"vars-on-top": 1,
		"yoda": 0
	}
};
