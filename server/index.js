/* @flow */
import express from 'express';
import graphQLHTTP from 'express-graphql';
import Schema from './schema';

const PORT = process.env.PORT || 8080;
const app = express();

app.use( '/graphql', graphQLHTTP( {
	schema: Schema,
	pretty: true,
	graphiql: true,
	formatError: error => ( {
		message: error.message,
		locations: error.locations,
		stack: error.stack,
	} ),
} ) );

app.use( express.static( 'public' ) );

app.listen( PORT, () => {
	console.log( `Listening on port ${PORT}` ); // eslint-disable-line no-console
} );
