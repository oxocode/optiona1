import redis from 'redis';

let client = null;

export const getClient = () => {
  if (client !== null) {
    return client;
  }

  client = redis.createClient({ prefix: 'wpgql:' });
  client.on('error', (err) => {
    console.log(`Error ${err}`);
  });

  client.on('connect', () => {
    console.log('Redis client connected');
  });

  return client;
};

export default redis;
