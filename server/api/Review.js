import { createLoader } from '../data';
import Model from './model';

let postLoader;
const path = '/reviews';

class Review extends Model {
	static getEndpoint() {
		return path;
	}

	static async load( id ) {
		const data = await postLoader.load( id );
		return data ? Object.assign( new Review(), data ) : null;
	}
}

postLoader = createLoader( Review );

export default Review;
