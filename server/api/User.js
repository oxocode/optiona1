import { createLoader } from '../data';
import Model from './model';

let userLoader;
const path = '/users';

class User extends Model {
	static getEndpoint() {
		return path;
	}

	static async load( id ) {
		const data = await userLoader.load( id );
		return data ? Object.assign( new User(), data ) : null;
	}
}

userLoader = createLoader( User );

export default User;
