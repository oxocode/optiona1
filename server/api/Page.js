import { createLoader } from '../data';
import Model from './model';

let pageLoader;
const path = '/pages';

class Page extends Model {
	static getEndpoint() {
		return path;
	}

	static async load( id ) {
		const data = await pageLoader.load( id );
		return data ? Object.assign( new Page(), data ) : null;
	}
}

pageLoader = createLoader( Page );

export default Page;
