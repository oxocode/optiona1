import { createLoader } from '../data';
import Model from './model';

let postLoader;
const path = '/posts';

class Post extends Model {
	static getEndpoint() {
		return path;
	}

	static async load( id ) {
		const data = await postLoader.load( id );
		return data ? Object.assign( new Post(), data ) : null;
	}
}

postLoader = createLoader( Post );

export default Post;
