import bodyParser from 'body-parser';
import responseTime from 'response-time';
import chalk from 'chalk';

// Create application/json parser.
const jsonParser = bodyParser.json();

export default function queryLogger() {
	return [
		jsonParser,
		( req, res, next ) => {
			const date = new Date();

			console.log( `--- ${chalk.green( `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}` )} ---` );
			console.log( chalk.red( 'query:\n' ) + req.body.query );
			console.log( chalk.red( 'variables:\n' ) + JSON.stringify( req.body.variables, null, 2 ) );
			console.log( '------- Done --------' );
			console.log( 'Response:', res.statusCode );
			Object.keys( req.headers ).forEach( k =>
				console.log( chalk.red( `${k}:` ), req.headers[k], )
			);
			next();
		},
		responseTime( ( req, res, time ) => {
			console.log( chalk.green( `Response time: ${Math.floor( time )}ms` ) );
		} )
	];
}
