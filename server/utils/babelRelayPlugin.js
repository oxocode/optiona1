const getBabelRelayPlugin = require( 'babel-relay-plugin' );
const schema = require( '../schema/build/schema.json' );

module.exports = getBabelRelayPlugin( schema.data );
