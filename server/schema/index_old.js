/* eslint-disable no-unused-vars, no-use-before-define */
import {
	GraphQLBoolean,
	GraphQLFloat,
	GraphQLID,
	GraphQLInt,
	GraphQLList,
	GraphQLNonNull,
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString
} from 'graphql';

import {
	connectionArgs,
	connectionDefinitions,
	connectionFromArray,
	fromGlobalId,
	globalIdField,
	mutationWithClientMutationId,
	nodeDefinitions
} from 'graphql-relay';

import Post from '../api/Post';
import Review from '../api/Review';
import Type from '../api/Type';
import User from '../api/User';

import {
	getUser,
	getPosts,
	getReviews
} from '../data/database';

/**
 * We get the node interface and field from the Relay library.
 *
 * The first method defines the way we resolve an ID to its object.
 * The second defines the way we resolve an object to its GraphQL type.
 */
const { nodeInterface, nodeField } = nodeDefinitions(
	( globalId ) => {
		const { type } = fromGlobalId( globalId );
		switch ( type ) {
		case 'Post':
			return Post.load( globalId );
		case 'Review':
			return Review.load( globalId );
		case 'Type':
			return Type.load( globalId );
		case 'User':
			return User.load( globalId );
		default:
			return null;
		}
	},
	( obj ) => {
		switch ( true ) {
		case obj instanceof PostType:
			return Post;
		case obj instanceof ReviewType:
			return Review;
		case obj instanceof UserType:
			return User;
		case obj instanceof TypeType:
			return Type;
		default:
			return null;
		}
	}
);

export { nodeInterface, nodeField };
/**
 * Define your own types here
 */

const UserType = new GraphQLObjectType( {
	name: 'User',
	description: 'Basically...Author',
	fields: () => ( {
		id: globalIdField( 'User' ),
		posts: {
			type: postConnection,
			description: 'Authored posts',
			args: connectionArgs,
			resolve: ( _, args ) => connectionFromArray( getPosts(), args )
		},
		reviews: {
			type: reviewConnection,
			description: 'Authored reviews',
			args: connectionArgs,
			resolve: ( _, args ) => connectionFromArray( getReviews(), args )
		},
		username: {
			type: GraphQLString,
			description: 'Author\'s username'
		},
		website: {
			type: GraphQLString,
			description: 'Author\'s website'
		}
	} ),
	interfaces: [nodeInterface]
} );

const TitleType = new GraphQLObjectType( {
	name: 'Title',
	fields: {
		rendered: {
			type: GraphQLString
		}
	}
} );

const ContentType = new GraphQLObjectType( {
	name: 'Content',
	fields: {
		rendered: {
			type: GraphQLString
		}
	}
} );

const PostType = new GraphQLObjectType( {
	name: 'Post',
	description: 'Posts',
	fields: () => ( {
		id: globalIdField( 'Post' ),
		title: {
			type: TitleType,
			description: 'Post title'
		},
		content: {
			type: ContentType,
			description: 'Post Content'
		},
		slug: {
			type: GraphQLString,
			description: 'Post slug'
		},
		link: {
			type: GraphQLString,
			description: 'Post link'
		},
		type: {
			type: GraphQLString,
			description: 'Post type'
		}
	} ),
	interfaces: [nodeInterface]
} );

const ReviewType = new GraphQLObjectType( {
	name: 'Review',
	description: 'Reviews',
	fields: () => ( {
		id: globalIdField( 'Review' ),
		title: {
			type: TitleType,
			description: 'Review title'
		},
		content: {
			type: ContentType,
			description: 'Review content'
		},
		slug: {
			type: GraphQLString,
			description: 'Review slug'
		},
		link: {
			type: GraphQLString,
			description: 'Review link'
		},
		type: {
			type: GraphQLString,
			description: 'Post type'
		}
	} ),
	interfaces: [nodeInterface]
} );

/**
 * Define your own connection types here
 */
const { connectionType: postConnection } = connectionDefinitions( { name: 'Post', nodeType: PostType } );
const { connectionType: reviewConnection } = connectionDefinitions( { name: 'Review', nodeType: ReviewType } );

/**
 * This is the type that will be the root of our query,
 * and the entry point into our schema.
 */
const queryType = new GraphQLObjectType( {
	name: 'Query',
	fields: () => ( {
		node: nodeField,
		// Add your own root fields here.
		viewer: {
			type: UserType,
			resolve: () => getUser( '1' )
		}
	} )
} );

/**
 * This is the type that will be the root of our mutations,
 * and the entry point into performing writes in our schema.
 */
const mutationType = new GraphQLObjectType( {
	name: 'Mutation',
	fields: () => ( {
		// Add mutations here.
	} )
} );

/**
 * Finally, we construct our schema (whose starting query type is the query
 * type we defined above) and export it.
 */
export default new GraphQLSchema( {
	query: queryType
	// Uncomment the following after adding some mutation fields:
	// mutation: mutationType
} );
