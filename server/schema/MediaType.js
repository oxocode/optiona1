import { GraphQLUnionType } from 'graphql';

import ImageType from './ImageType';

const MediaType = new GraphQLUnionType( {
	name: 'Media',
	types: [ImageType],
} );

export default MediaType;
