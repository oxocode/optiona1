import { GraphQLObjectType } from 'graphql';

import rendered from './helpers/rendered';

const Caption = new GraphQLObjectType( {
	name: 'Caption',
	description: 'The caption for the object.',
	fields: {
		...rendered,
	},
} );

export default Caption;
