import {
	GraphQLInterfaceType,
	GraphQLNonNull,
	GraphQLID,
	GraphQLList,
} from 'graphql';

import MetaType from './MetaType';

import { description, caption, link, source_url, title, type } from './helpers/fields';

const MediaInterface = new GraphQLInterfaceType( {
	name: 'MediaInterface',
	fields: {
		id: {
			type: new GraphQLNonNull( GraphQLID ),
			description: 'Unique identifier for the object.',
		},
		...type,
		...link,
		...title,
		...description,
		...caption,
		...source_url,
		meta: { type: new GraphQLList( MetaType ) },
		post: {
			type: new GraphQLNonNull( GraphQLID ),
			description: 'The ID for the associated post of the attachment.',
		},
	},
} );

export default MediaInterface;
