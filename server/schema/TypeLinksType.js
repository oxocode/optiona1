import {
	GraphQLObjectType,
	GraphQLList,
} from 'graphql';

import Link from './LinkType';

const TypeLinks = new GraphQLObjectType( {
	name: 'TypeLinks',
	description: 'The links for a type.',
	fields: {
		collection: { type: new GraphQLList( Link ) },
		items: {
			type: new GraphQLList( Link ),
			resolve: type => type._links['wp:items'],
		}
	},
} );

export default TypeLinks;
