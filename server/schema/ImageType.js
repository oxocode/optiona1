import {
	GraphQLObjectType,
} from 'graphql';

import MediaInterface from './MediaInterface';
import mediaFields from './MediaFieldsType';
import ImageDetails from './ImageDetailsType';

const ImageType = new GraphQLObjectType( {
	name: 'Image',
	description: 'An object.',
	interfaces: [MediaInterface],
	isTypeOf( media ) {
		return media.mime_type.indexOf( 'image' ) === 0;
	},
	fields: {
		...mediaFields,
		media_details: { type: ImageDetails },
	},
} );

export default ImageType;
