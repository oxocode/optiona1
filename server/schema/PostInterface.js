import {
	GraphQLInterfaceType,
	GraphQLNonNull,
	GraphQLID,
	GraphQLList,
} from 'graphql';

import UserType from './UserType';
import MediaType from './MediaType';
import MetaType from './MetaType';

import { slug, link, title, content, date, modified, type } from './helpers/fields';

const PostInterface = new GraphQLInterfaceType( {
	name: 'PostInterface',
	fields: {
		id: {
			type: new GraphQLNonNull( GraphQLID ),
			description: 'Unique identifier for the object.',
		},
		...date,
		...modified,
		...slug,
		...type,
		...link,
		...title,
		...content,
		meta: { type: new GraphQLList( MetaType ) },
		author: { type: UserType },
		featured_media: { type: MediaType },
	},
} );

export default PostInterface;
