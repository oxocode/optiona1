import {
	GraphQLObjectType,
	GraphQLList,
} from 'graphql';

import Link from './LinkType';

const MediaLinks = new GraphQLObjectType( {
	name: 'MediaLinks',
	description: 'The links for the media.',
	fields: {
		self: { type: new GraphQLList( Link ) },
		collection: { type: new GraphQLList( Link ) },
		about: { type: new GraphQLList( Link ) },
	},
} );

export default MediaLinks;
