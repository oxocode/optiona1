import { GraphQLObjectType } from 'graphql';
import {
	connectionArgs,
	connectionDefinitions,
} from 'graphql-relay';

import MediaType from './MediaType';
import { loadEdges } from '../utils';
import Media from '../api/Media';

const { connectionType: MediaConnection } =
	connectionDefinitions( { nodeType: MediaType } );

const MediaCollectionType = new GraphQLObjectType( {
	name: 'MediaCollection',
	description: 'Collection of media based on cursors.',
	fields: {
		results: {
			type: MediaConnection,
			args: connectionArgs,
			description: 'A list of results',
			resolve: loadEdges( Media ),
		},
	},
} );

export default MediaCollectionType;
