import {
	nodeDefinitions,
	fromGlobalId,
} from 'graphql-relay';

// import MediaType from './MediaType';
import PageType from './PageType';
import PostType from './PostType';
import TypeType from './TypeType';
import UserType from './UserType';

import postQueries from './PostQuery';
import userQueries from './UserQuery';
import pageQueries from './PageQuery';
import typeQueries from './TypeQuery';
// import mediaQueries from 'MediaQuery';

import Post from '../api/Post';
// import Review from '../api/Review';
import Page from '../api/Page';
// import Media from '../api/Media';
import Type from '../api/Type';
import User from '../api/User';

/**
 * We get the node interface and field from the Relay library.
 *
 * The first method defines the way we resolve an ID to its object.
 * The second defines the way we resolve an object to its GraphQL type.
 */
const { nodeInterface, nodeField } = nodeDefinitions(
	( globalId ) => {
		const { type } = fromGlobalId( globalId );
		switch ( type ) {
		case 'Post':
			return Post.load( globalId );
			// case 'Review':
			// 	return Review.load( globalId );
		case 'Page':
			return Page.load( globalId );
			// case 'Media':
			// 	return Media.load( globalId );
		case 'Type':
			return Type.load( globalId );
		case 'User':
			return User.load( globalId );
		default:
			return null;
		}
	},
	( obj ) => {
		switch ( true ) {
		case obj instanceof PostType:
			return Post;
			// case obj instanceof ReviewType:
			// 	return Review;
		case obj instanceof PageType:
			return Page;
			// case obj instanceof MediaType:
			// 	return Media;
		case obj instanceof TypeType:
			return Type;
		case obj instanceof UserType:
			return User;
		default:
			return null;
		}
	}
);

export default {
	node: nodeField,
	...postQueries,
	...userQueries,
	...pageQueries,
	...typeQueries,
//	...mediaQueries,
};
