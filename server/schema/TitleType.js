import { GraphQLObjectType } from 'graphql';

import rendered from './helpers/rendered';

const TitleType = new GraphQLObjectType( {
	name: 'Title',
	description: 'The title for an object.',
	fields: {
		...rendered,
	},
} );

export default TitleType;
