import {
	GraphQLObjectType,
	GraphQLList,
	GraphQLString
} from 'graphql';

import TypeLinks from './TypeLinksType';

import { globalIdField, description, slug, name } from './helpers/fields';

const TypeType = new GraphQLObjectType( {
	name: 'Type',
	description: 'A post type.',
	fields: {
		id: globalIdField(),
		...description,
		...name,
		...slug,
		taxonomies: {
			type: new GraphQLList( GraphQLString ),
			description: 'Taxonomies associated with post type.',
		},
		rest_base: {
			type: GraphQLString,
			description: 'REST base route for the post type.',
		},
		_links: { type: TypeLinks },
	},
} );

export default TypeType;
