import {
	GraphQLObjectType,
} from 'graphql';

import PostInterface from './PostInterface';

import { globalIdField, slug, link, title, content, date, modified, type } from './helpers/fields';
import { featuredMedia } from './helpers/mediafields';
import author from './helpers/author';
import metaField from './helpers/metafields';

const PageType = new GraphQLObjectType( {
	name: 'Page',
	description: 'An object.',
	interfaces: [PostInterface],
	isTypeOf( page ) {
		return page.type === 'page';
	},
	fields: () => ( {
		id: globalIdField(),
		...date,
		...modified,
		...slug,
		...type,
		...link,
		...title,
		...content,
		...author,
		featured_media: featuredMedia(),
		meta: metaField(),
	} ),
} );

export default PageType;
