import POST_ORDERBY from 'enum/PostOrderby';

import PostCollectonType from './PostCollectionType';
import PostType from './PostType';
import { pagination, filter, date, author, slug } from './QueryArgs';

import Post from '../api/Post';
import { itemResolver } from '../utils';

export default {
	posts: {
		type: PostCollectonType,
		args: {
			...pagination,
			...filter,
			...date,
			...author,
			...slug,
			orderby: { type: POST_ORDERBY },
		},
		resolve: ( root, args ) => ( {
			args: {
				...args,
			},
		} ),
	},
	post: itemResolver( PostType, Post ),
};
