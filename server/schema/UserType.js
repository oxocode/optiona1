import {
	GraphQLObjectType,
	GraphQLList,
} from 'graphql';

import UserAvatar from './UserAvatarType';

import { globalIdField, slug, description, name, link } from './helpers/fields';

const UserType = new GraphQLObjectType( {
	name: 'User',
	description: 'An object.',
	fields: {
		id: globalIdField(),
		...name,
		...description,
		...link,
		...slug,
		avatar_urls: {
			type: new GraphQLList( UserAvatar ),
			description: 'Avatar URLs for the user.',
			resolve: user => (
				Object.keys( user.avatar_urls ).map( key => ( {
					size: key,
					url: user.avatar_urls[key],
				} ) )
			),
		},
	},
} );

export default UserType;
