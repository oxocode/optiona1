import { GraphQLSchema } from 'graphql';
import Query from './TypeQuery';

const Schema = new GraphQLSchema( {
	query: Query,
} );

export default Schema;
