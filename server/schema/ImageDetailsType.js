import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
} from 'graphql';

const ImageDetails = new GraphQLObjectType( {
	name: 'ImageDetails',
	description: 'The details for the media.',
	fields: {
		width: { type: GraphQLInt },
		height: { type: GraphQLInt },
		file: { type: GraphQLString },
	},
} );

export default ImageDetails;
