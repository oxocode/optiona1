import {
	GraphQLID,
	GraphQLNonNull,
	GraphQLString,
} from 'graphql';

import TitleType from '../TitleType';
import ContentType from '../ContentType';

export const globalIdField = () => ( {
	type: new GraphQLNonNull( GraphQLID ),
	description: 'Unique identifier for the object.',
	resolve: data => data.getID(),
} );

export const content = {
	content: {
		type: ContentType,
	},
};

export const date = {
	date: {
		type: GraphQLString,
		description: 'The date the object was published, in the timezone of the site.',
	},
	date_gmt: {
		type: GraphQLString,
		description: 'The date the object was published, as GMT.',
	},
};

export const description = {
	description: {
		type: GraphQLString,
		description: 'The object description.',
	},
};
//
// export const getMatchDescription = {
// 	type: GraphQLString,
// 	description: 'The object description.',
// };

export const link = {
	link: {
		type: GraphQLString,
		description: 'URL to the object.',
	},
};

export const modified = {
	modified: {
		type: GraphQLString,
		description: 'The date the object was modified, in the timezone of the site.',
	},
	modified_gmt: {
		type: GraphQLString,
		description: 'The date the object was modified, as GMT.',
	},
};

export const name = {
	name: {
		type: GraphQLString,
		description: 'HTML title for the object.',
	},
};

export const slug = {
	slug: {
		type: GraphQLString,
		description: 'An alphanumeric identifier for the object unique to its type.',
	},
};

export const title = {
	title: {
		type: TitleType,
		description: 'Title field.',
	},
};

export const type = {
	type: {
		type: GraphQLString,
		description: 'Type of Post for the object.',
	},
};
