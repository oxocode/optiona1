import { GraphQLString } from 'graphql';
import { toGlobalId } from 'graphql-relay';

import Description from '../DescriptionType';
import CaptionType from '../CaptionType';
import MediaType from '../MediaType';
import Media from '../../api/Media';

export const featuredMedia = () => ( {
	type: MediaType,
	description: 'The featured media for the object.',
	resolve: ( { featured_media } ) => {
		if ( featured_media > 0 ) {
			return Media.load( toGlobalId( 'Media', featured_media ) );
		}
		return null;
	},
} );

export const description = {
	description: {
		type: GraphQLString,
		description: 'Alternative text to display when attachment is not displayed.',
	},
};

export const caption = {
	caption: {
		type: CaptionType,
	},
};

export const alt_text = {
	alt_text: {
		type: GraphQLString,
		description: 'Alternative text to display when attachment is not displayed.',
	},
};

export const mime_type = {
	mime_type: {
		type: GraphQLString,
		description: 'The attachment MIME type.',
	},
};

export const source_url = {
	source_url: {
		type: GraphQLString,
		description: 'URL to the original attachment file.',
	},
};
