import { GraphQLString } from 'graphql';

import UserCollectionType from './UserCollectionType';
import UserType from './UserType';
import User from '../api/User';
import { itemResolver } from '../utils';
import { pagination, filter, slug } from './QueryArgs';

export default {
	users: {
		type: UserCollectionType,
		args: {
			...pagination,
			...filter,
			...slug,
			roles: {
				type: GraphQLString,
				description: 'Limit result set to users matching at least one specific role provided. Accepts csv list or single role (value or comma-separated values).',
			},
		},
		resolve: ( root, args ) => ( { args } ),
	},
	user: itemResolver( UserType, User ),
};
