import { GraphQLObjectType } from 'graphql';

import rendered from './helpers/rendered';

const Content = new GraphQLObjectType( {
	name: 'Content',
	description: 'The content for the object.',
	fields: {
		...rendered,
	},
} );

export default Content;
