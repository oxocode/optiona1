import MediaCollectionType from './MediaCollectionType';
import MediaType from './MediaType';
import Media from '../api/Media';
import { itemResolver } from '../utils';
import { pagination, filter, slug } from './QueryArgs';

export default {
	media: {
		type: MediaCollectionType,
		args: {
			...pagination,
			...filter,
			...slug,
		},
		resolve: ( root, args ) => ( { args } ),
	},
	medium: itemResolver( MediaType, Media ),
};
