import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
} from 'graphql';

const UserAvatar = new GraphQLObjectType( {
	name: 'Avatar',
	description: 'Avatar info.',
	fields: {
		size: { type: GraphQLInt },
		url: { type: GraphQLString },
	},
} );

export default UserAvatar;
