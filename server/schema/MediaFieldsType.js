import {
	GraphQLNonNull,
	GraphQLID,
} from 'graphql';
import { toGlobalId } from 'graphql-relay';

import MediaLinks from './MediaLinksType';

import metaField from './helpers/metafields';
import { globalIdField, slug, guid, link, title, type, date, modified } from './helpers/fields';
import { description, caption, alt_text, media_type, mime_type, source_url } from './helpers/mediafields';
import author from './helpers/author';

export default {
	id: globalIdField(),
	...date,
	...guid,
	...modified,
	...slug,
	...type,
	...link,
	...title,
	...author,
	meta: metaField(),
	_links: { type: MediaLinks },
	...description,
	...caption,
	...alt_text,
	...media_type,
	...mime_type,
	...source_url,
	post: {
		type: new GraphQLNonNull( GraphQLID ),
		description: 'The ID for the associated post of the attachment.',
		resolve: data => toGlobalId( 'Post', data.post ),
	},
};
