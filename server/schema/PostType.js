import {
	GraphQLObjectType,
} from 'graphql';

import PostInterface from './PostInterface';
import PostLinks from './PostLinksType';

import { globalIdField, slug, link, title, content, date, modified, type } from './helpers/fields';
import { featuredMedia } from './helpers/mediafields';
import author from './helpers/author';
import metaField from './helpers/metafields';

const PostType = new GraphQLObjectType( {
	name: 'Post',
	description: 'A read-only post object.',
	interfaces: [PostInterface],
	isTypeOf( post ) {
		return post.type === 'post';
	},
	fields: {
		id: globalIdField(),
		...date,
		...modified,
		...slug,
		...type,
		...link,
		...title,
		...content,
		...author,
		featured_media: featuredMedia(),
		meta: metaField(),
		_links: { type: PostLinks },
	},
} );

export default PostType;
