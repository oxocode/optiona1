import PageCollectionType from './PageCollectionType';
import PageType from './PageType';
import Page from '../api/Page';
import { itemResolver } from '../utils';
import { pagination, filter, hierarchical, slug } from './QueryArgs';

export default {
	pages: {
		type: PageCollectionType,
		args: {
			...pagination,
			...filter,
			...hierarchical,
			...slug,
		},
		resolve: ( root, args ) => ( { args } ),
	},
	page: itemResolver( PageType, Page ),
};
