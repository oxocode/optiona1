import PostCollectonType from './PostCollectionType';
import PostType from './PostType';
import Post from '../api/Post';
import { itemResolver } from '../utils';
import { pagination, filter, date, author, slug } from './QueryArgs';

export default {
	posts: {
		type: PostCollectonType,
		args: {
			...pagination,
			...filter,
			...date,
			...author,
			...slug,
		},
		resolve: ( root, args ) => ( {
			args: {
				...args,
				sticky: false,
			},
		} ),
	},
	post: itemResolver( PostType, Post ),
};
