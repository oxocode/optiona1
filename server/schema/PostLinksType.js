import {
	GraphQLObjectType,
	GraphQLList,
} from 'graphql';

import Link from './LinkType';

const PostLinks = new GraphQLObjectType( {
	name: 'PostLinks',
	description: 'The links for a post.',
	fields: {
		self: { type: new GraphQLList( Link ) },
		collection: { type: new GraphQLList( Link ) },
		about: { type: new GraphQLList( Link ) },
		attachment: {
			type: new GraphQLList( Link ),
			resolve: post => post._links['wp:attachment'],
		},
	},
} );

export default PostLinks;
