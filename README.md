optionA.1
=====================
An exploratory prototype that renders the client-side views using React, GraphQL, and Relay.

Google Doc: https://docs.google.com/document/d/1Nv-wKOq0neyZz9SdoR9Q20UyJBZAKTecMpAJHmciIN0/

**The header, footer, fonts and styling assets are originally from the TimesTested MVP project.
Many of the assets are hardcoded and currently load from those project files.
They will be updated as this theme is updated.**

## Screenshot

## Stack
- React
  - [X] React
  - [X] React Router
- Relay
  - [X] React Relay
- GraphQL  
  - [X] GraphQL
  - [X] Apollo
- Webpack
  - [X] Webpack
  - [X] Webpack Dev Middleware
  - [X] Webpack Hot Middleware
- Linting
  - [X] Eslint
- Styles
  - [X] Node Sass
  - [X] Node Bourbon
  - [X] Node Neat
  - [X] Sass Loader
  - [X] Style Loader
  - [X] PostCSS Loader

## Usage
```
git clone ssh://git@bitbucket.org/oxocode/optiona1.git
cd optiona1

```
## Installation
------
```
npm install -g yarn
yarn
npm run build - Generates the theme styling and React.js assets.
```
### Configuration

### Workflow
------
```
npm run update-schema
npm start
```

APP: http://localhost:8080/  
GraphQL: http://localhost:8080/graphql

## ToDo (time permitting)
