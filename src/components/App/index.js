/* @flow */
import React from 'react';

const App = React.createClass( {
	render() {
		const app = this.props;

		console.log( 'App', app );
		return (
			<div className="app">
				{app.children}
			</div>
		);
	}
} );

export default App;
