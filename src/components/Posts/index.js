import React, { Component } from 'react';
import PostItem from '../PostItem';

type Props = {
  infinite: true,
};

export default class Posts extends Component {

  props: Props;

  render() {
    const {
			posts: {
				results: {
					edges: posts,
					pageInfo: {
						hasNextPage,
					},
				},
			},
		} = this.props;

    return (
      <div>
        <h2>Posts</h2>
        <hr />
        <div className="portfolio-max-width">
          {posts.map(({ cursor, node }) => (
            <PostItem key={cursor} post={node}{...node} />
              ))}
        </div>
        {this.props.infinite && hasNextPage && (
          <button
            onClick={() => {
              this.props.relay.setVariables({
                total: this.props.relay.variables.total + 10,
              });
            }}
          >
                  LOAD MORE
              </button>
          )}
      </div>
    );
  }
}
