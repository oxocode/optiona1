import React, { Component, PropTypes } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

class SinglePost extends Component {
	render() {
		console.log( props );
		const post = props;

		return (
			<div>
				<div className="mdl-grid portfolio-max-width" key={post.id}>
					<div className="mdl-cell mdl-card mdl-shadow--4dp portfolio-card">
						<div className="mdl-card__title">
							<h2 className="mdl-card__title-text" dangerouslySetInnerHTML={this.rawHTML( post.title )} />
						</div>
						<div className="mdl-card__supporting-text">
							<span dangerouslySetInnerHTML={this.rawHTML( post.content )} />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

SinglePost.propTypes = {
	relay: React.PropTypes.object.isRequired,
	post: React.PropTypes.object.isRequired,
	id: React.PropTypes.number.isRequired,
	data: React.PropTypes.shape( {
		loading: PropTypes.bool.isRequired,
		post: PropTypes.object,
		title: React.PropTypes.object.isRequired,
	} ).isRequired,
};

// We use the gql tag to parse our query string into a query document
const CurrentPost = gql`
query {
	        post {
 fragment on Post {
						title{
                                rendered
                            },
                            content{
                                rendered
                            },
                            author,
                            date
                            }
                            }
                        }`;
const CurrentPostData = graphql( CurrentPost )( SinglePost );

