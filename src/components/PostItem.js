import React from 'react';
import { Link } from 'react-router';

const PostItem = ( { post } ) => {

	console.log( post );

	return (
		<div className="list-item-component">
			<div className="mdl-card__title">
				<h2 className="mdl-card__title-text">
					<Link to={`/blog/${post.slug}`} dangerouslySetInnerHTML={{ __html: post.title.rendered }} />
				</h2>
			</div>
		</div>
	);
};

PostItem.propTypes = {
	post: React.PropTypes.object.isRequired
};

export default PostItem;
