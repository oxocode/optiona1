/* @flow */
import React, { Component } from 'react';

export default class NotFound extends Component {

	render() {
		return (
			<div className="container">
				<h2>nope!</h2>
			</div>
		);
	}
}
