/* @flow */
import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import Posts from './containers/Posts';
import NotFound from './components/NotFound';
import postsQuery from './queries/postsQuery';

export default (
	<Route path="/" component={App}>
		<IndexRoute component={Posts} queries={postsQuery} />
		<Route path="*" component={NotFound} />
	</Route>
);
