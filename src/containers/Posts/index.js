import Relay from 'react-relay';
import Posts from '../../components/Posts';

export default Relay.createContainer(Posts, {
  initialVariables: {
    total: 10,
  },
  fragments: {
    posts: () => Relay.QL`
            fragment on PostCollection {
                results(first: $total) {
                    edges {
                        node {
                            id,
	                        title{
		                        rendered
	                        },
	                        slug,
	                        content{
		                        rendered
	                        },
	                        author,
	                        link,
	                        featured_media
                        }
                        cursor
                    }
                    pageInfo {
                        hasNextPage
                    }
                }
            }
		`,
  },
});
