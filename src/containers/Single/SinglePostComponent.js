import React, { Component } from 'react';

type Props = {
    relay: React.PropTypes.object.isRequired,
    location: React.PropTypes.object,
    post: React.PropTypes.object.isRequired,
    id: React.PropTypes.number.isRequired,
};
export default class SinglePost extends Component {

  rawHTML(html) {
    return { __html: html, sanitize: true };
  }

  render() {
    const post = this.props;

    const {
			id,
			title: { rendered: title },
			author,
			date,
			content: { rendered: content },
		} = this.props;

    return (
      <div>
        <div className="mdl-grid portfolio-max-width" key={id}>
          <div className="mdl-cell mdl-card mdl-shadow--4dp portfolio-card">
            <div className="mdl-card__title">
              <h2 className="mdl-card__title-text" dangerouslySetInnerHTML={this.rawHTML(title)} />
            </div>
            <div className="mdl-card__supporting-text">
              <span dangerouslySetInnerHTML={this.rawHTML(content)} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
