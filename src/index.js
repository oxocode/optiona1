import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { browserHistory, Router, applyRouterMiddleware } from 'react-router';
import useRelay from 'react-router-relay';
import routes from './routes';

import 'isomorphic-fetch';
import './assets/sass/style.scss';
const rootEl = document.getElementById( 'main' );

const networkInterface = createNetworkInterface( {
	uri: 'http://localhost:8080/graphql',
} );

const client = new ApolloClient( {
	networkInterface,
	initialState: window.__APOLLO_STATE__,
	ssrForceFetchDelay: 100,
	addTypename: true,
	dataIdFromObject: ( result ) => {
		if ( result.id && result.__typename ) {
			return result.__typename + result.id;
		}
		return null;
	}
} );

ReactDOM.render(
	<ApolloProvider client={client}>
		<Router
			environment={Relay.Store}
			history={browserHistory}
			render={applyRouterMiddleware( useRelay )}
		>
			{routes}
		</Router>
	</ApolloProvider>,
	rootEl,
);


if ( module.hot ) {
	// If we're hot reloading.
	module.hot.accept();
}
