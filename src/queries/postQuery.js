import Relay from 'react-relay';

export default {
  post: Component => Relay.QL`
        query {
	        posts {
                ${Component.getFragment('post')}
            }
        }`,
};
