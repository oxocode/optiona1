import Relay from 'react-relay';

export default {
  posts: Component => Relay.QL`
        query {
	        posts {
                ${Component.getFragment('posts')}
            }
        }`,
};
