/* eslint key-spacing:0 spaced-comment:0 */
const path = require( 'path' );
const debug = require( 'debug' )( 'app:config:project' );
const ip = require( 'ip' );

/**
 * Creates default webpack configuration.
 */
debug( 'Creating default webpack configuration...' );

const config = {
	/**
	 * Emotes `development` environment, if environment is not provided.
	 */
	env: process.env.NODE_ENV || 'development',

	/**
	 * Project structure.
	 */
	path_base: path.resolve( __dirname, '..' ),
	dir_client: 'src',
	dir_server: 'server',
	dir_public: 'public',
	dir_dist: 'dist',
	dir_lib: 'lib',

};

/**
 * Environment globals.
 * @type {{[process.env]: {NODE_ENV: (*)}, NODE_ENV: (*), __DEV__: boolean, __PROD__: boolean, __BASENAME__}}
 */
config.globals = {
	'process.env': {
		NODE_ENV: config.env,
	},
	'NODE_ENV': config.env,
	'__DEV__': config.env === 'development',
	'__PROD__': config.env === 'production',
	'__BASENAME__': JSON.stringify( process.env.BASENAME || '' ),
};

/**
 * Configuration Utilities.
 * @returns {*} base path
 */
function base() {
	const args = [config.path_base].concat( [].slice.call( arguments ) );
	return path.resolve( ...args );
}

/**
 * File path Configuration.
 * @type {{base: base, client: (()), public: (()), dist: (())}}
 */
config.paths = {
	base,
	client: base.bind( null, config.dir_client ),
	server: base.bind( null, config.dir_server ),
	public: base.bind( null, config.dir_public ),
	dist: base.bind( null, config.dir_dist ),
	lib: base.bind( null, config.dir_lib )
};

module.exports = config;
