/**
 * This file configures the development web server,
 * which supports hot reloading and synchronized testing.
 * historyApiFallback() is required for react-router browserHistory
 * Reference: https://github.com/BrowserSync/browser-sync/issues/204#issuecomment-102623643
 */
import browserSync from 'browser-sync';
import historyApiFallback from 'connect-history-api-fallback';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import {clientConfig} from './webpack.config.babel';

const bundler = webpack( clientConfig );

/**
 * Run Browsersync and use middleware for Hot Module Replacement.
 */
browserSync( {
	port: 3000,
	ui: {
		port: 3001
	},
	server: {
		baseDir: 'src',
		middleware: [
			historyApiFallback(),
			webpackDevMiddleware( bundler, {
				publicPath: clientConfig.output.publicPath,
				noInfo: false,
				stats: {
					assets: false,
					colors: true,
					version: false,
					hash: false,
					timings: false,
					chunks: false,
					chunkModules: false
				},
			} ),
			webpackHotMiddleware( bundler, {
				log: console.log,
				noInfo: false,
				reload: true,
				overlay: true,
				path: '/__webpack_hmr',
				heartbeat: 10 * 1000,
			} )
		]
	},
	/**
	 * 	No need to watch '*.js' files here, because webpack will take care of it for us.
	 * 	Webpack will also handle full page reloads if HMR doesn't work.
	 */
	files: [
		'public/index.html'
	]
} );
