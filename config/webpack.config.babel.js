const webpack = require( 'webpack' );
const path = require( 'path' );
const debug = require( 'debug' )( 'app:config:webpack' );
const project = require( './dev.config' );
const cssnano = require( 'cssnano' );
const bourbon = require( 'node-bourbon' ).includePaths;
const neat = require( 'node-neat' ).includePaths;
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );

const extractCSS = new ExtractTextPlugin( 'style.css' );
const __DEV__ = project.globals.__DEV__;

export default () => {
	const clientConfig = {
		context: path.resolve( __dirname, '..' ),
		entry: {
			app: [
				'babel-polyfill',
				project.paths.client( 'index.js' ),
			],
		},
		target: 'web',
		output: {
			path: project.paths.public(),
			filename: '[name].bundle.js',
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					include: project.paths.client(),
					exclude: /node_modules/,
					use: 'babel-loader',
				},
				{
					test: /\.scss$/,
					include: project.paths.client( 'assets/sass' ),
					use: ExtractTextPlugin.extract( {
						fallback: 'style-loader',
						use: [
							'css-loader',
							'sass-loader',
							{
								loader: 'postcss-loader',
								query: {
									sourceMap: true,
								}
							}
						]
					} ),
				},
				{
					test: /\.woff(\?.*)?$/,
					use: 'url-loader?limit=10000&mimetype=application/font-woff',
					include: project.paths.client( 'assets/fonts' ),
				},
				{
					test: /\.woff2(\?.*)?$/,
					use: 'url-loader?limit=10000&mimetype=application/font-woff2',
					include: project.paths.client( 'assets/fonts' ),
				},
				{
					test: /\.otf(\?.*)?$/,
					use: 'file-loader?limit=10000&mimetype=font/opentype',
					include: project.paths.client( 'assets/fonts' ),
				},
				{
					test: /\.ttf(\?.*)?$/,
					use: 'url-loader?limit=10000&mimetype=application/octet-stream',
					include: project.paths.client( 'assets/fonts' ),
				},
				{
					test: /\.eot(\?.*)?$/,
					use: 'file-loader',
					include: project.paths.client( 'assets/fonts' ),
				},
				{
					test: /\.svg(\?.*)?$/,
					use: 'url-loader?limit=10000&mimetype=image/svg+xml',
					include: project.paths.client( 'assets/img' ),
				},
				{
					test: /\.(png|jpg)$/,
					use: 'url-loader?limit=8192',
					include: project.paths.client( 'assets/img' ),
				}
			],
		},
		devtool: __DEV__ ? 'cheap-module-eval-source-map' : 'cheap-module-source-map',
		resolve: {
			extensions: ['.json', '.js', '.jsx', '.scss'],
			modules: [
				'src',
				'node_modules',
			],
		},
		stats: {
			colors: true,
			reasons: true
		},
		plugins: [
			new webpack.LoaderOptionsPlugin( {
				options: {
					progress: true,
					eslint: {
						configFile: project.paths.base( '.eslintrc.js' ),
						failOnError: true,
						quiet: true,
					},
					sassLoader: {
						includePaths: [].concat( bourbon, neat )
					},
				}
			} ),
			extractCSS,
			new webpack.NoEmitOnErrorsPlugin(),
		],
	};

	/**
	 * Webpack development plugin configuration.
	 */
	if ( __DEV__ ) {
		debug( `Enabling plugins for live ${process.env.NODE_ENV} (HMR, NoErrors).` );
		clientConfig.plugins.push(
			new webpack.HotModuleReplacementPlugin(),
			new webpack.LoaderOptionsPlugin( {
				options: {
					postcss: [
						cssnano( {
							autoprefixer: {
								add: true,
								remove: true,
								browsers: ['last 2 versions']
							},
							discardComments: {
								removeAll: true
							},
							discardUnused: false,
							mergeIdents: false,
							reduceIdents: false,
							safe: true,
							sourcemap: true
						} )
					]
				}
			} ),
		);
	} else {
		/**
		 * Production plugin configuration.
		 */
		debug( `Enabling webpack plugins for ${process.env.NODE_ENV}` );
		clientConfig.plugins.push(
			new webpack.LoaderOptionsPlugin( {
				minimize: true,
				postcss: [
					cssnano( {
						autoprefixer: {
							add: true,
							remove: true,
							browsers: ['last 2 versions']
						},
						safe: true,
						sourcemap: true
					} )
				]
			} ),
			new webpack.optimize.OccurrenceOrderPlugin(),
			new webpack.optimize.UglifyJsPlugin( {
				compress: {
					warnings: false
				}
			} )
		);
	}

	const serverConfig = {
		entry: {
			server: [
				'babel-polyfill',
				project.paths.server( 'index.js' ),
			],
		},
		target: 'node',
		output: {
			path: project.paths.lib(),
			filename: 'server.js',
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					use: 'babel-loader',
					exclude: /node_modules/,
				},
			],
		},
		plugins: [
			new webpack.LoaderOptionsPlugin( { minimize: true } ),
			new webpack.optimize.UglifyJsPlugin( { sourceMap: false } ),
		],
		devtool: 'cheap-module-source-map',
		resolve: {
			modules: [
				'src',
				'server',
				'node_modules',
			],
		},
		stats: {
			colors: true,
			reasons: true
		},
	};

	return __DEV__ ? clientConfig : [serverConfig, clientConfig];
};
